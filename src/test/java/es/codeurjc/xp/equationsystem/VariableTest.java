package es.codeurjc.xp.equationsystem;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class VariableTest{

    @Test
    public void variableIsEqualToOtherVariableTest() {
        Variable oneX = new VariableBuilder().value(1).name("x").build();
        Variable otherOneX = new VariableBuilder().value(1).name("x").build();
        assertTrue(oneX.equal(otherOneX));
    }
    
    @Test
    public void variableIsNotEqualToOtherVariableTest() {
        Variable oneX = new VariableBuilder().value(1).name("x").build();
        Variable threeY = new VariableBuilder().value(3).name("y").build();
        assertFalse(oneX.equal(threeY));
    }
    
    @Test
    public void clonTest() {
        Variable threeZ = new VariableBuilder().value(3).name("z").build();
        Term cloneThreeZ = threeZ.clon();
        assertTrue(threeZ.equal(cloneThreeZ));
        assertTrue(threeZ != cloneThreeZ);
    }
    
    @Test
    public void hasNameTest() {
        Variable variable = new VariableBuilder().name("x").build();
        assertTrue(variable.hasName("x"));
    }

    @Test
    public void hasNoNameTest() {
        Variable variable = new VariableBuilder().name("x").build();
        assertFalse(variable.hasName("z"));
    }

    @Test
    public void hasNameSetTest() {
        Set<String> nameSet = new HashSet<String>();
        nameSet.add("x");
        nameSet.add("y");
        nameSet.add("z");
        Variable variable = new VariableBuilder().name("y").build();
        assertTrue(variable.hasName(nameSet));
    }

    @Test
    public void hasNotNameSetTest() {
        Set<String> nameSet = new HashSet<String>();
        nameSet.add("x");
        nameSet.add("z");
        Variable variable = new VariableBuilder().name("y").build();
        assertFalse(variable.hasName(nameSet));
    }

    @Test
    public void toStringTest() {
        Variable variable = new VariableBuilder().value(7).name("y").build();
        assertEquals("7.0y", variable.toString());
    }
}
