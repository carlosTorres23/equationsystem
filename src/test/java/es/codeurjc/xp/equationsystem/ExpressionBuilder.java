package es.codeurjc.xp.equationsystem;

import java.util.HashSet;
import java.util.Set;

public class ExpressionBuilder {

    Set<Constant> constantSet = new HashSet<Constant>();
    Set<Variable> variableSet = new HashSet<Variable>();
    
    public ExpressionBuilder _(float value) {
        constantSet.add(new Constant(value));
        return this;
    }

    public ExpressionBuilder _(float value, String name) {
        variableSet.add(new Variable(value, name));
        return this;
    }

    public Expression build() {
        Expression expression = new Expression();
        for (Constant constant : constantSet) {
            expression.add(constant);
        }
        for (Variable variable : variableSet) {
            expression.add(variable);
        }
        return expression;
    }
    
}
