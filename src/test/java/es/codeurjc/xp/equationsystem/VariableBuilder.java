package es.codeurjc.xp.equationsystem;

public class VariableBuilder {

    private float value = 7;
    private String name = "x";
    
    public VariableBuilder value(float value) {
        this.value = value;
        return this;
    }

    public VariableBuilder name(String name) {
        this.name = name;
        return this;
    }
    
    public Variable build() {
        return new Variable(value, name);
    }
    
}
