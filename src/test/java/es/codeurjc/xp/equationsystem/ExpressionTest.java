package es.codeurjc.xp.equationsystem;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ExpressionTest {

    @Test
    public void multiplyByZeroTest() {
        Expression expression = new ExpressionBuilder().
                _(4)._(5, "x").build();
        expression.multiply(0);
        assertEquals(0, expression.getValue(), 0.001);
        assertEquals(0, expression.getValue("x"), 0.001);
    }

    @Test
    public void multiplyByValueTest() {
        Expression expression = new ExpressionBuilder().
                _(4)._(7)._(5, "x").build();
        expression.multiply(3.3f);
        Expression expectedExpression = new ExpressionBuilder().
                _(4 * 3.3f)._(7 * 3.3f)._(5 * 3.3f, "x").build();
        assertTrue(expression.equal(expectedExpression));
    }
    
    @Test
    public void simplifyWithOnlyConstantsTest() {
        Expression expression = new ExpressionBuilder().
                _(4)._(-7.4f)._(99.12f).build();
        expression.simplify();
        Expression expectedExpression = new ExpressionBuilder().
                _(4 - 7.4f + 99.12f).build();
        assertTrue(expression.equal(expectedExpression));
    }

    @Test
    public void simplifyWithoutConstantsTest() {
        Expression expression = new ExpressionBuilder().
                _(4, "x")._(-8.93f, "w")._(234, "z").build();
        expression.simplify();
        Expression expectedExpression = new ExpressionBuilder().
                _(4, "x")._(-8.93f, "w")._(234, "z").build();
        assertTrue(expression.equal(expectedExpression));
    }

    @Test
    public void simplifyWithConstantsAndVariablesTest() {
        Expression expression = new ExpressionBuilder().
                _(4)._(-7.4f)._(5.1f, "x")._(99.12f)._(-8.32f, "x").build();
        expression.simplify();
        Expression expectedExpression = new ExpressionBuilder().
                _(4 + -7.4f + 99.12f)._(5.1f, "x")._(-8.32f, "x").build();
        assertTrue(expression.equal(expectedExpression));
    }

    @Test
    public void simplifyVariableTest() {
        Expression expression = new ExpressionBuilder().
                _(4)._(-7.4f)._(7, "x")._(14.23f, "y")._(-3.2f, "x").build();
        expression.simplify("x");
        Expression expectedExpression = new ExpressionBuilder().
                _(4)._(-7.4f)._(7 + -3.2f, "x")._(14.23f, "y").build();
        assertTrue(expression.equal(expectedExpression));
    }

    @Test
    public void simplifyVariableNotPresentTest() {
        Expression expression = new ExpressionBuilder().
                _(-7.4f)._(-8.93f, "w")._(234, "z").build();
        expression.simplify("x");
        Expression expectedExpression = new ExpressionBuilder().
                _(-7.4f)._(-8.93f, "w")._(234, "z").build();
        assertTrue(expression.equal(expectedExpression));
    }

    @Test
    public void simplifyVariableNotPresentWithoutVariablesTest() {
        Expression expression = new ExpressionBuilder().
                _(-7.4f)._(9.7f).build();
        expression.simplify("x");
        Expression expectedExpression = new ExpressionBuilder().
                _(-7.4f)._(9.7f).build();
        assertTrue(expression.equal(expectedExpression));
    }
    
    @Test
    public void applyValueToNameTest() {
        Expression expression = new ExpressionBuilder().
                _(-7.4f)._(-8.93f, "x")._(234, "y").build();
        expression.apply("x", 21);
        Expression expectedExpression = new ExpressionBuilder().
                _(-7.4f)._(-8.93f * 21)._(234, "y").build();
        assertTrue(expression.equal(expectedExpression));
    }
    
    @Test
    public void hasNameFoundTest() {
        Expression expression = new ExpressionBuilder().
                _(-7.4f)._(-8.93f, "x")._(234, "y").build();
        assertTrue(expression.hasName("y"));
    }

    @Test
    public void getValueTwoConstantsTest() {
        Expression expression = new ExpressionBuilder().
                _(-7.4f)._(2.3f)._(-8.93f, "x")._(234, "y").build();
        assertEquals(-7.4, expression.getValue(), 0.001);
        }

    @Test
    public void getValueWithoutConstantsTest() {
        Expression expression = new ExpressionBuilder().
                _(-8.93f, "x")._(234, "y").build();
        assertEquals(0, expression.getValue(), 0.001);
        }
}
