package es.codeurjc.xp.equationsystem;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class TermTest {
    
    @Test
    public void multiplyTest() {
        Variable variable = new VariableBuilder().value(3).build();
        variable.multiply(5);
        assertEquals(15, variable.getValue(), 0.0001);
    }

    @Test
    public void hasNameTest() {
        Constant constant = new Constant(10);
        assertFalse(constant.hasName("y"));
    }

    @Test
    public void hasNameSetTest() {
        Set<String> nameSet = new HashSet<String>();
        nameSet.add("x");
        nameSet.add("y");
        Constant constant = new Constant(10);
        assertFalse(constant.hasName(nameSet));
    }
    
    @Test
    public void constantNotEqualToVariableTest() {
        Constant constant = new Constant(23);
        Variable variable = new VariableBuilder().value(23).build();
        assertFalse(constant.equal(variable));
    }
}
