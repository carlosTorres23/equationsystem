package es.codeurjc.xp.equationsystem;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class ConstantTest {
    
    private Constant seven;
    
    @Before
    public void setup() {
        seven = new Constant(7);
    }
    
    @Test
    public void constantIsEqualToOtherConstantTest() {
        Term otherSeven = new Constant(7);
        assertTrue(seven.equal(otherSeven));
    }
    
    @Test
    public void constantIsNotEqualToOtherConstantTest() {
        Term twentyThree = new Constant(23);
        assertFalse(seven.equal(twentyThree));
    }
    
    @Test
    public void clonTest() {
        Term clone = seven.clon();
        assertTrue(seven.equal(clone));
        assertTrue(seven != clone);
    }
    
}
