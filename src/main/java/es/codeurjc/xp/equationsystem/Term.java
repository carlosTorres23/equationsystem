package es.codeurjc.xp.equationsystem;

import java.util.Set;

public abstract class Term {
    
    protected float value;
    
    public Term(float value) {
        this.value = value;
    }
    
    public float getValue() {
        return value;
    }
    public boolean equal(Term term) {
        return this.getClass() == term.getClass();
    }

    public abstract Term clon();
    
    public boolean hasName(String name) {
        return false;
    }

    public boolean hasName(Set<String> nameSet) {
        return false;
    }

    public void multiply(float factor) {
        this.value = this.value * factor;
    }

    @Override
    public String toString() {
        return Float.toString(value);
    }

    public abstract void dispatch(TermVisitor termVisitor);
}
