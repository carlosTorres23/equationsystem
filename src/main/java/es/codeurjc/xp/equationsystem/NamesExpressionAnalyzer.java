package es.codeurjc.xp.equationsystem;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NamesExpressionAnalyzer extends TermVisitor {

    private List<Term> termList = new ArrayList<Term>();
    private Set<String> nameSet = new HashSet<String>();
    
    public NamesExpressionAnalyzer(List<Term> termList) {
        super();
        this.termList = termList;
    }

    public Set<String> getNameSet() {
        for (Term term : termList) {
            term.dispatch(this);
        }
        return nameSet;
    }
    
    @Override
    public void visit(Variable variable) {
        nameSet.add(variable.getName());
    }

    @Override
    public void visit(Constant constant) {
        // nothing to do
    }

}
