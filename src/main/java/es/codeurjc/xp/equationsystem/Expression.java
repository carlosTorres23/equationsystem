package es.codeurjc.xp.equationsystem;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Expression {

    private List<Term> termList= new ArrayList<Term>();
    private NamesExpressionAnalyzer namesAnalyzer = new NamesExpressionAnalyzer(termList);
    
    public void add(Term term) {
        termList.add(term);
    }

    public void add(Expression expression) {
        for (Term term : expression.termList) {
            termList.add(term);
        }
    }

    public Set<String> getNameSet() {
        return namesAnalyzer.getNameSet();
    }

    public float getValue() {
        for (Term term : termList) {
            if (!term.hasName(namesAnalyzer.getNameSet())) {
                return term.getValue();
            }
        }
        return 0;
    }

    public float getValue(String name) {
        float value = 0;
        for (Term term : termList) {
            if (term.hasName(name)) {
                value = value + term.getValue();
            }
        }
        return 0;
    }

    public void multiply(float factor) {
        for (Term term : termList) {
            term.multiply(factor);
        }
    }

    public void simplify() {
        float value = 0;
        List<Term> newTermList = new ArrayList<Term>();
        for (Term term : termList) {
            if (!term.hasName(namesAnalyzer.getNameSet())) {
                value = value + term.getValue();
            } else {
                newTermList.add(term);
            }
        }
        if (value != 0) {
            newTermList.add(new Constant(value));
        }
        termList = newTermList;
    }

    public void simplify(String name) {
        float value = 0;
        List<Term> newTermList = new ArrayList<Term>();
        for (Term term : termList) {
            if (term.hasName(name)) {
                value = value + term.getValue();
            } else {
                newTermList.add(term);
            }
        }
        if (value != 0) {
            newTermList.add(new Variable(value, name));
        }
        termList = newTermList;
    }

    public boolean equal(Expression otherExpression) {
        for (Term term : termList) {
            if (!otherExpression.contains(term)) {
                return false;
            }
        }
        for (Term term : otherExpression.termList) {
            if (!this.contains(term)) {
                return false;
            }
        }
        return true;
    }
    
    private boolean contains(Term termToCheck) {
        boolean result = false;
        for (Term term : termList) {
            if (term.equal(termToCheck)) {
                result = true;
            }
        }
        return result;
    }

    public void apply(String name, float value) {
        for (Term term : termList) {
            if (term.hasName(name)) {
                termList.set(termList.indexOf(term), 
                        new Constant(term.getValue() * value));
            }
        }
    }

    public boolean hasName(String name) {
        for (Term term : termList) {
            if (term.hasName(name)) {
                return true;
            }
        }
        return false;
    }
}
