package es.codeurjc.xp.equationsystem;

public abstract class TermVisitor {
    public abstract void visit(Variable variable);
    public abstract void visit(Constant constant);
}
