package es.codeurjc.xp.equationsystem;

import java.util.Set;

public class Variable extends Term {

    private String name;
    
    public Variable(float value, String name) {
        super(value);
        assert name != null;
        assert !name.isEmpty();
        this.name = name;
    }

    @Override
    public boolean equal(Term term) {
        return super.equal(term) &&
               this.value == term.value &&
               this.name.equals(((Variable)term).name);
    }

    @Override
    public Term clon() {
        return new Variable(this.value, this.name);
    }

    @Override
    public boolean hasName(String name) {
        return this.name.equals(name);
    }

    @Override
    public boolean hasName(Set<String> nameSet) {
        return nameSet.contains(this.name);
    }

    @Override
    public String toString() {
        return super.toString() + name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public void dispatch(TermVisitor termVisitor) {
        termVisitor.visit(this);
    }
}
